package com.molekule.moscanner.base;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.molekule.moscanner.R;
import com.molekule.moscanner.utils.Constants;
import com.molekule.moscanner.utils.LoggerUtils;
import com.molekule.moscanner.utils.ProgressDialogUtil;

public abstract class MainAbsFragmentActivity extends AppCompatActivity {

    /** Progress dialog */
    protected ProgressDialogUtil dialogUtil = null;
    /** Check if fragment can respond on back button press   */
    protected int backStack = 0;
    /** Validate back press button action  */
    protected boolean isCanChangeFragment = true;
    /** Permission id */
    private int REQUEST_BLUETOOTH = 1;
    /** Bluetooth Adapter instance */
    private BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            LoggerUtils.log("bluetoothAdapter not enabled");
            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBT, REQUEST_BLUETOOTH);
        } else {
            LoggerUtils.log("bluetoothAdapter enabled");
        }
    }

    /**
     * Hide/Show progress_dialog
     *
     * @param show
     */
    public void showProgress(boolean show, String message) {
        if (show) {
            dialogUtil = ProgressDialogUtil.newInstance(message);
            dialogUtil.show(getSupportFragmentManager(), null);
        } else {
            dialogUtil.dismiss();
        }
    }

    /**
     * Add new fragment without backStack
     * @param fragment
     */
    public void startNewFragment(Fragment fragment){
        // Check if fragment not doing any BE calling
        if (this.isCanChangeFragment) {
            // Get fragment Manager
            FragmentManager fm = getSupportFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            // Initialize fragment transaction
            FragmentTransaction ft = fm.beginTransaction();
            // Replace with fragment content
            ft.replace(R.id.simple_fragment, fragment).commit();
            // Reset stack
            this.backStack = 0;
        }
    }

    /**
     * Add new fragment to stack.
     * @param fragment
     */
    public void addFragmentToStack(Fragment fragment) {
        // Check if fragment not doing any BE calling
        if (this.isCanChangeFragment) {
            // Initialize fragment transaction
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace with fragment content
            ft.replace(R.id.simple_fragment, fragment);
            // Animation on change
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

            // Add fragment to stack (back button memory)
            ft.addToBackStack(fragment.toString());
            ft.commit();
            // Can do back action
            this.backStack++;
        }
    }

    /**
     * This method is required for all devices running API23+
     * Android must programmatically check the permissions for bluetooth. Putting the proper permissions
     * in the manifest is not enough.
     */
    protected void checkBTPermissions() {

        int permissionCheck = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        permissionCheck += checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheck != 0) {

            this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Constants.PERMISSIONS_ID); //Any number
        }

    }

    public BluetoothAdapter getBluetoothAdapter() {
        return bluetoothAdapter;
    }

}
