package com.molekule.moscanner;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.molekule.moscanner.base.MainAbsFragment;
import com.molekule.moscanner.utils.Session;

public class BLEDetailFragment extends MainAbsFragment {

    private DeviceItem item;
    private TextView tvDeviceNameValue, tvDeviceManufacturerValue, tvDeviceUDIDValue;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.item = Session.getInstance().getSelectedDevice();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_layout, container, false);

        tvDeviceNameValue = view.findViewById(R.id.tvDeviceNameValue);
        tvDeviceManufacturerValue = view.findViewById(R.id.tvDeviceManufacturerValue);
        tvDeviceUDIDValue = view.findViewById(R.id.tvDeviceUDIDValue);

        tvDeviceNameValue.setText(getString(R.string.item_value, item.getName()));
        tvDeviceManufacturerValue.setText(getString(R.string.item_value, item.getManufacturer()));
        tvDeviceUDIDValue.setText(getString(R.string.item_value, item.getAddress()));


        return view;
    }

}
