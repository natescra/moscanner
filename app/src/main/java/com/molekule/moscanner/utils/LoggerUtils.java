package com.molekule.moscanner.utils;

import android.util.Log;

public abstract class LoggerUtils {

    private static String TAG = "MoScanner LOG -->";

    public static void log(String message) {
        Log.v(TAG, message);
    }
}
