package com.molekule.moscanner.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ProgressDialogUtil extends DialogFragment {

    //region CLASS VARIABLES
    /** Alert message */
    private String msg = null;
    /** Alert dialog builder */
    private static ProgressDialogUtil alert = null;
    /** Alert dialog instance */
    private ProgressDialog dialog = null;
    //endregion

    /**
     * Class Constructor
     * @param msg
     * @return
     */
    public static ProgressDialogUtil newInstance(String msg) {
        alert = new ProgressDialogUtil();
        alert.msg = msg;

        return alert;
    }

    /**
     * Create Alert Dialog Instance
     */
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage(msg);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    @Override
    public void onPause() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        super.onPause();
    }
}