package com.molekule.moscanner.utils;

import com.molekule.moscanner.DeviceItem;

public class Session {

    private static Session instance = null;

    private Session() {}

    public static Session getInstance() {

        if (instance == null)
            instance = new Session();

        return instance;
    }

    private DeviceItem selectedDevice;
    public DeviceItem getSelectedDevice() { return selectedDevice; }
    public void setSelectedDevice(DeviceItem data) { this.selectedDevice = data; }
}
