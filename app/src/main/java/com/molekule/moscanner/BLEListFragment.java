package com.molekule.moscanner;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.molekule.moscanner.adapter.BLEAdapter;
import com.molekule.moscanner.base.MainAbsFragment;
import com.molekule.moscanner.base.MainAbsFragmentActivity;
import com.molekule.moscanner.utils.LoggerUtils;

public class BLEListFragment extends MainAbsFragment {

    private BLEAdapter viewAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager viewManager;

    private BluetoothAdapter bluetoothAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        recyclerView = view.findViewById(R.id.viewBLERecycler);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bluetoothAdapter = ((MainAbsFragmentActivity) getActivity()).getBluetoothAdapter();
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        this.initList();
        this.startScan();
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);

        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_scan) {
            if (bluetoothAdapter.isDiscovering()) {
//                item.setTitle(R.string.action_scan);
                finishScan();
            } else {
//                item.setTitle(R.string.action_stop);
                startScan();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Broadcast Receiver for listing devices that are not yet paired
     * -Executed by btnDiscover() method.
     */
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            LoggerUtils.log("ACTION FOUND: " + action);

            if (action.equals(BluetoothDevice.ACTION_FOUND)){
                BluetoothDevice device = intent.getParcelableExtra (BluetoothDevice.EXTRA_DEVICE);
                LoggerUtils.log("onReceive: " + device.getName() + ": " + device.getAddress());
                DeviceItem item = new DeviceItem(device.getName(), device.getAddress(), device.getBluetoothClass().toString(), device.createBond(), device.getUuids());
                viewAdapter.addItem(item);
                viewAdapter.notifyDataSetChanged();
            } else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                finishScan();
            }
        }
    };

    private void initList() {
        viewManager = new LinearLayoutManager(getActivity());
        viewAdapter = new BLEAdapter(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(viewManager);
        recyclerView.setAdapter(viewAdapter);
    }

    private void startScan() {
        ((MainAbsFragmentActivity) getActivity()).showProgress(true, getString(R.string.loading_scan));
        LoggerUtils.log("Looking for unpaired devices.");

        bluetoothAdapter.startDiscovery();
        IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        getActivity().registerReceiver(receiver, discoverDevicesIntent);

        discoverDevicesIntent = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        getActivity().registerReceiver(receiver, discoverDevicesIntent);

    }

    private void finishScan() {
        ((MainAbsFragmentActivity) getActivity()).showProgress(false, null);
    }
}
