package com.molekule.moscanner;

import android.os.ParcelUuid;

public class DeviceItem {

    private String name;
    private String address;
    private String manufacturer;
    private boolean connectable;
    private ParcelUuid[] udids;

    public DeviceItem(String name, String address, String manufacturer, boolean connectable, ParcelUuid[] uuids) {
        this.name = name;
        this.address = address;
        this.connectable = connectable;
        this.manufacturer = manufacturer;
        this.udids = uuids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isConnectable() {
        return connectable;
    }

    public void setConnectable(boolean connectable) {
        this.connectable = connectable;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public ParcelUuid[] getUdids() {
        return udids;
    }

    public void setUdids(ParcelUuid[] udids) {
        this.udids = udids;
    }
}
