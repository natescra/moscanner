package com.molekule.moscanner;

import android.os.Bundle;

import com.molekule.moscanner.base.MainAbsFragmentActivity;

public class MainActivity extends MainAbsFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        this.checkBTPermissions();
        this.startNewFragment(new BLEListFragment());
    }
}
