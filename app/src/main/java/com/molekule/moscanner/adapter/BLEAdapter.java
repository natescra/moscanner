package com.molekule.moscanner.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.molekule.moscanner.BLEDetailFragment;
import com.molekule.moscanner.DeviceItem;
import com.molekule.moscanner.R;
import com.molekule.moscanner.base.MainAbsFragmentActivity;
import com.molekule.moscanner.utils.LoggerUtils;
import com.molekule.moscanner.utils.Session;

import java.util.ArrayList;
import java.util.List;

public class BLEAdapter extends RecyclerView.Adapter<BLEAdapter.BLEViewHolder>{

    List<DeviceItem> list;
    Context context;

    public BLEAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<DeviceItem>();
    }

    @NonNull
    @Override
    public BLEViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ble_item, viewGroup, false);
        BLEViewHolder holder = new BLEViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BLEViewHolder holder, final int position) {
        holder.tvName.setText(context.getString(R.string.item_value, list.get(position).getName()));
        holder.tvStatus.setText((list.get(position).isConnectable()) ? context.getString(R.string.item_connectable) : context.getString(R.string.item_non_connectable));
        holder.btnConnect.setVisibility(list.get(position).isConnectable() ? View.VISIBLE : View.GONE);
        holder.btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.getInstance().setSelectedDevice(list.get(position));
                ((MainAbsFragmentActivity) context).addFragmentToStack(new BLEDetailFragment());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addItem(DeviceItem item) {
        LoggerUtils.log("Received item: " + item.getName());

        if (!checkDeviceExist(item))
            this.list.add(item);
    }

    private boolean checkDeviceExist(DeviceItem item) {
        boolean exist = false;

        for (DeviceItem device :list) {
            if (device.getName().equals(item.getName())) {
                exist = true;
            }
        }

        return exist;
    }

    class BLEViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvStatus;
        Button btnConnect;

        public BLEViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvDeviceName);
            tvStatus = itemView.findViewById(R.id.tvDeviceStatus);
            btnConnect = itemView.findViewById(R.id.btnDeviceConnect);
        }
    }
}
